module main

go 1.16

require (
	github.com/FortAwesome/Font-Awesome v4.7.0+incompatible // indirect
	github.com/boydkelly/asciidocsy v0.0.0-20210617194708-e3e5d31eacc4 // indirect
	gitlab.com/boydkelly/content.git v0.0.0-20210829193020-e34b1e69ff39 // indirect
	gitlab.com/boydkelly/julakan-docs v0.0.0-20210814235010-92d97f276dba // indirect
	gitlab.com/boydkelly/julakan-docs.git v0.0.0-20210829101911-99a4a91a55a9 // indirect
)
