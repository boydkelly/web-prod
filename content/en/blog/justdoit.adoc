---
categories: [ "English in French"]
date: 2016-04-21T02:50:28+02:00
image: /images/nike-5a2c1cf1488404.728018091512840433297.jpg
tags: [ "english", "language", "french"]
title: 'Just do it !'
---

= Just do it !
:author: Boyd Kelly
:date: 2016-04-21T02:50:28+02:00
:lang: fr 
:description: Just do it !
:tags: ["french", "language"]
:categories: ["English in French"]
:keywords: [traduction, translation, nike, anglais, english, french, français, "anglais en français" ]
:imagesdir: /images/
include::./locale/attributes.adoc[fr]

image:nike-5a2c1cf1488404.728018091512840433297.jpg[role="right", width="50%"]

Le slogan publicitaire de Nike n'est pas façile à traduire en français. Que veut dire cette expression? Le mot 'just' est un faux ami. Quant il est utilisé comme adjectif, il peut souvent se traduire par 'juste' en voulant dire équitable.

A just decision::
Une décision juste

Ou bien c'est souven traduit par 'seulement'.

Just a little::
Juste un peu

Mais 'Just do it' n'est pas si évident. Google traduit 'just do' par 'il suffit de faire.

Just take this medicine...:: Il suffit de prendre ce médicament...

C'est bon mais en anglais il y a une nuance qui sera perdue dans notre slogan de Nike. Dans le contexte ou on utilise 'just' suivi d'une verbe, on peut trouver souvent une raison justement de ne pas faire ou agir (selon la verbe utilisée).

Donc l'adverbe 'just' sous-entends 'malgré les obstacles', en dépit des circonstances, peu importe ce que disent les autres. etc.

Bien sûr dans le slogan de Nike, le context c'est le sport. Donc, malgré tes limitations physiques, peu importe ce que disent les autres, (y compris ton opposant?), malgré la peur! Tu peux tout surmonter!

C'est un slogan très bien pensée!

NOTE: Disclaimer: I wear Adidas.
