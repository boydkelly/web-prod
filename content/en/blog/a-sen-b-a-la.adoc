---
categories: [ "Julakan"]
date: 2018-08-07T07:05:01+00:00
tags: [ "jula", "language"]
title: "a sen b'a la"
---

= a sen b'a la
:author: Boyd Kelly
:date: 2018-08-07T07:05:01+00:00
:description: A sen b'a la
:lang: fr 
:categories: ["Julakan"]
:tags: ["jula language"]
include::./locale/attributes.adoc[]

== He is involved !

[cols="1"]
|===
|*a sen b’a la*
|He is involved.  /  He's implicated.
|(He has his foot in it.)
|===
