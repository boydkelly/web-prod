---
author: 'Boyd Kelly'
categories: [ "Julakan"]
date: 2020-12-06T20:18:52
draft: false
image: /images/w100.svg
tags: [ "reference", "julakan"]
title: 'The 100 most common words in Ivory Coast Jula'
---

= The 100 most common words in Ivory Coast Jula 
:author: Boyd Kelly
:date: 2020-12-06T20:18:52
:draft: false
:description:
:filename: 2020-12-06-w100.adoc
:imagesdir: /assets/images/
:keywords: ["mots", "clés"]
:tags: ["reference", "julakan"]
:categories: ["Julakan"]
:lang: fr 
include::locale/attributes.adoc[]

image::w100.svg["image", width=100%]

.Words in order of frequency.footnote:[Kodi, Editions Africaines, 1981]
[width="100%", cols="10,20,20,20,30", format=tsv,frame="bottom",options="header",stripes="even"]
|====
include::{includedir}w100.tsv[]
|====

