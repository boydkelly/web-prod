---
categories: [ "Julakan"]
date: 2020-05-08T14:07:30
draft: false
hyperlink: https://www.jw.org/dyu
image: /images/jw.org.jpeg
tags: [  "jula", "langue"]
title: 'title: jw.org en bambara, français, et jula'
type: notification
---

= title: jw.org en bambara, français, et jula 
:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: resources.adoc
:description: Ressources for Jula Learners
:imagesdir: /images/
:type: post
:tags: [ "jula", "langue"]
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula, "jw.org", mandenkan, bible]
:lang: fr 


[width="100%", cols="3", frame="topbot", options="none", stripes="even"]
|===
3+|Ces sites contiennent des articles, vidéos et pistes audio que vous pouvez comparez avec les versions correspondantes en bambara ou en français.
|link:https://www.jw.org/bm[jw.org en bambara] 
|link:https://www.jw.org/fr[jw.org en français]
|link:https://www.jw.org/dyu[jw.org en jula] 
|===
