#!/usr/bin/bash
pushd content/en
find . -name "*.adoc" -exec sed -i -e "1,20{s/'\[/\[ /g; s/\]'/\]/g; s/''//g}" {} \;
find . -name "*.adoc" -exec sed -i -e "1,20{s/catégo/catego/g}" {} \;
popd
pushd content/dyu
find . -name "*.adoc" -exec sed -i -e "1,20{s/'\[/\[ /g; s/\]'/\]/g; s/''//g}" {} \;
popd
pushd content/fr
find . -name "*.adoc" -exec sed -i -e "1,20{s/'\[/\[ /g; s/\]'/\]/g; s/''//g}" {} \;
