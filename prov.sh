#!/bin/bash
in=./data/proverbs.tsv
out=./public/proverb.txt
dest=

for x in shuf; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

shuf -n 1 $in | awk -F"\t" '{ print $0 }' > $out

