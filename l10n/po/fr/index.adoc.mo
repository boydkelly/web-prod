��          �      �       0     1     4  �   I          +    1  �   6  �   "  	   �     �     �     �  �   �     �     �  �        �  	   �  4  �  "  2  �   U	     �	     �	     �	     �	                   	                        
               10 2018-05-21T21:18:02Z <div src="https://docs.google.com/forms/d/e/1FAIpQLSdw6yhla0-mmVrAWeLcHM2lBKHvKZre4uiiiGCjvaG30x22Qg/viewform?embedded=true" width="700" height="1000" frameborder="0" marginheight="0" marginwidth="0">
</div>
 <i class=""></i> About I have hosted websites, managed email and dns servers, coded, and taught a MS Exchange course at the University of British Columbia.  I have a lot of experience in virtual technologies on Linux, Amazon and GCP. Working remotely with full confidence is a given. In short, I have done extensive volunteer service including IT work, managed the help desk for the Canadian Passport Office, and worked in a large corporate IT department.  That’s experience in volunteer, public, and private sectors. This static website is hosted on Gitlab, and created by me using http://www.asciidoctor.org[Asciidoctor], http://hugo.io[Hugo], and http://neovim.io[neovim].  The DNS is hosted on Amazon Route 53. Who am I? about en page Project-Id-Version: 
PO-Revision-Date: 2021-08-29 21:04+0000
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: en
X-Generator: Poedit 3.0
 10 2018-05-21T21:18:02Z <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeSaw5q-3yyLxe8mH5WNYHDJ8TgUFOaQNzvJdXRx4SJdc-QmA/viewform?embedded=true" width="700" height="1000" frameborder="0" marginheight="0" marginwidth="0"></iframe>
 <i class=""></i> À propos Au cours de route j'ai hébergé des sites web, géré les servers mail et DNS et enseigné les cours de Microsoft Exchange à l'Université de la Columbie Britanique.  Avec beaucoup d'experience dans les technologies virtuelles sur Linux, Amazon et GCE, je peux travailler à distance avec pleine confiance. En sommaire, j'ai fait du bénévolat extensif en afrique, tout en travaillant avec la technologie.  J'ai supervisé departement de support technique pour le Bureau des Passports du Canada à l'ouest et ensuite travaillé dans le departement informatique d'une grande société de logiciel. Ce site web static est hébérgé sur Gitlab, créée par moi avec Hugo , Asciidoctor et neovim .  Le DNS est hébergé chez Amazon Route 53. Qui suis-je ? about fr page 