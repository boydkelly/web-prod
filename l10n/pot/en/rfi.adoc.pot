# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-04-22 13:37+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: categories
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "[\"Julakan\" ]"
msgstr ""

#. type: YAML Front Matter: type
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/ankataa.com.adoc:1
#: content/en/resources/bebiphilip.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#: content/en/page/2020-11-24-money.adoc:1
#, no-wrap
msgid "notification"
msgstr ""

#. type: YAML Front Matter: date
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "2020-05-08T14:07:30"
msgstr ""

#. type: Attribute :lang:
#: content/en/resources/ankataa.com.adoc:21
#: content/en/resources/bebiphilip.adoc:22 content/en/resources/bible.adoc:21
#: content/en/resources/mandenkan.com.adoc:20 content/en/resources/rfi.adoc:20
#: content/en/resources/jw.org.adoc:22
#: content/en/resources/fonts_google.adoc:20
#: content/en/resources/fonts_ngalonci.adoc:19
#: content/en/page/2020-11-24-money.adoc:25
#, no-wrap
msgid "en "
msgstr ""

#. type: YAML Front Matter: tags
#: content/en/resources/bebiphilip.adoc:1
#: content/en/resources/bebiphilip.adoc:19 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#, no-wrap
msgid "[ \"jula\", \"language\"]"
msgstr ""

#. type: YAML Front Matter: hyperlink
#: content/en/resources/rfi.adoc:1
#, no-wrap
msgid "https://www.rfi.fr/ma"
msgstr ""

#. type: YAML Front Matter: image
#: content/en/resources/rfi.adoc:1
#, no-wrap
msgid "/images/rfi_mandenkan.jpg"
msgstr ""

#. type: Title =
#: content/en/resources/rfi.adoc:1 content/en/resources/rfi.adoc:11
#, no-wrap
msgid "Radio France International Mandenkan"
msgstr ""

#. type: Table
#: content/en/resources/rfi.adoc:31
#, no-wrap
msgid ""
"2+|Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.\n"
"Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.\n"
"10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.\n"
"|Web site: link:http://rfi.fr/ma[rfi]\n"
"|\n"
msgstr ""
