# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-04-22 13:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: categories
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "[\"Julakan\" ]"
msgstr ""

#. type: YAML Front Matter: tags
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/fonts_google.adoc:1
#, no-wrap
msgid "[\"jula\", \"language\"]"
msgstr ""

#. type: YAML Front Matter: type
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/ankataa.com.adoc:1
#: content/en/resources/bebiphilip.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#: content/en/page/2020-11-24-money.adoc:1
#, no-wrap
msgid "notification"
msgstr ""

#. type: YAML Front Matter: date
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "2020-05-08T14:07:30"
msgstr ""

#. type: Attribute :lang:
#: content/en/resources/ankataa.com.adoc:20
#: content/en/resources/bebiphilip.adoc:22 content/en/resources/bible.adoc:21
#: content/en/resources/mandenkan.com.adoc:20 content/en/resources/rfi.adoc:20
#: content/en/resources/jw.org.adoc:21
#: content/en/resources/fonts_google.adoc:20
#: content/en/resources/fonts_ngalonci.adoc:19
#: content/en/page/2020-11-24-money.adoc:25
#, no-wrap
msgid "en "
msgstr ""

#. type: YAML Front Matter: hyperlink
#: content/en/resources/fonts_google.adoc:1
#, no-wrap
msgid "https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom"
msgstr ""

#. type: YAML Front Matter: image
#: content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "/images/polices.png"
msgstr ""

#. type: YAML Front Matter: title
#: content/en/resources/fonts_google.adoc:1
#, no-wrap
msgid "Google web fonts for manding languages"
msgstr ""

#. type: Title =
#: content/en/resources/fonts_google.adoc:11
#, no-wrap
msgid "Google web fonts for manding languages "
msgstr ""

#. type: Plain text
#: content/en/resources/fonts_google.adoc:24
msgid ""
"If your are an African web developer go to link:https://fonts.google.com/?"
"preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni"
"%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Google Fonts] ` to "
"check if your favorite web font contains the appropriate unicode characters "
"to display your language."
msgstr ""
