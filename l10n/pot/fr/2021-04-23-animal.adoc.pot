# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-08-29 20:43+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :catégories:
#: content/fr/blog/mogo.adoc:1 content/fr/blog/mogo.adoc:15
#: content/fr/blog/a-sen-b-a-la.adoc:1 content/fr/blog/a-sen-b-a-la.adoc:13
#: content/fr/blog/ngalonci.adoc:1 content/fr/blog/anki.adoc:1
#: content/fr/blog/2020-12-06-w100.adoc:1
#: content/fr/blog/2020-12-06-w100.adoc:20
#: content/fr/blog/2021-04-23-animal.adoc:1
#: content/fr/blog/2021-04-23-animal.adoc:20
#: content/fr/page/2018-07-30-alpha-jula.adoc:1
#: content/fr/page/2018-07-30-alpha-jula.adoc:25
#, no-wrap
msgid "[\"Julakan\"]"
msgstr ""

#. type: Attribute :lang:
#: content/fr/blog/mogo.adoc:13 content/fr/blog/fonts.adoc:19
#: content/fr/blog/nvim-plugins.adoc:19 content/fr/blog/vimplug.adoc:16
#: content/fr/blog/a-sen-b-a-la.adoc:12 content/fr/blog/ysa.adoc:12
#: content/fr/blog/justdoit.adoc:12 content/fr/blog/datally.adoc:14
#: content/fr/blog/clavier-ivoirien.adoc:15 content/fr/blog/fontlist.adoc:18
#: content/fr/blog/2020-12-06-w100.adoc:21
#: content/fr/blog/2021-01-02-adoc-vim.adoc:19
#: content/fr/blog/2021-04-23-animal.adoc:21
#, no-wrap
msgid "fr "
msgstr ""

#. type: YAML Front Matter: draft
#: content/fr/blog/fonts.adoc:1 content/fr/blog/nvim-plugins.adoc:1
#: content/fr/blog/proverb.adoc:1 content/fr/blog/ngalonci.adoc:1
#: content/fr/blog/fontlist.adoc:1 content/fr/blog/sb-bashrc.adoc:1
#: content/fr/blog/2020-12-06-w100.adoc:1
#: content/fr/blog/2021-01-02-adoc-vim.adoc:1
#: content/fr/blog/2021-04-23-animal.adoc:1
#, no-wrap
msgid "false"
msgstr ""

#. type: YAML Front Matter: author
#: content/fr/blog/nvim-plugins.adoc:1
#: content/fr/blog/ponctuation-francaise.adoc:1
#: content/fr/blog/2020-12-06-w100.adoc:1
#: content/fr/blog/2021-01-02-adoc-vim.adoc:1
#: content/fr/blog/2021-04-23-animal.adoc:1
#: content/fr/page/2018-07-30-alpha-jula.adoc:1
#, no-wrap
msgid "Boyd Kelly"
msgstr ""

#. type: Attribute :tags:
#: content/fr/blog/2020-12-06-w100.adoc:19
#: content/fr/blog/2021-04-23-animal.adoc:1
#: content/fr/blog/2021-04-23-animal.adoc:19
#, no-wrap
msgid "[\"référence\", \"julakan\"]"
msgstr ""

#. type: YAML Front Matter: date
#: content/fr/blog/2021-04-23-animal.adoc:1
#, no-wrap
msgid "2021-04-23"
msgstr ""

#. type: YAML Front Matter: image
#: content/fr/blog/2021-04-23-animal.adoc:1
#, no-wrap
msgid "/images/animal.svg"
msgstr ""

#. type: YAML Front Matter: title
#: content/fr/blog/2021-04-23-animal.adoc:1
#, no-wrap
msgid "Les animaux en Jula de Côte d'Ivoire"
msgstr ""

#.  image::animal.svg[image,width=100%]
#. type: Block title
#: content/fr/blog/2021-04-23-animal.adoc:11
#: content/fr/blog/2021-04-23-animal.adoc:26
#, no-wrap
msgid "Les animaux en Jula de Côte d'Ivoire "
msgstr ""

#. type: Table
#: content/fr/blog/2021-04-23-animal.adoc:30
#, no-wrap
msgid "include::{includedir}animal.tsv[]\n"
msgstr ""
