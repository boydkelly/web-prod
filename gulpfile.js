var gulp = require('gulp'); 
const { src, dest } = require("gulp");
gulp.task('copyfonts', function(done) {
  gulp.src('./node_modules/@fontsource/**/*latin-ext-{400,700}*.{css,woff2,woff}')
    .pipe(gulp.dest('./static/font'));
  gulp.src('./node_modules/@openfonts/**/{*latin-ext-{400,700}*.{woff2,woff},index.css}')
    .pipe(gulp.dest('./static/font'));
  done();
});
