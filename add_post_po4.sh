#!/usr/bin/bash

#[type: asciidoc] content/fr/post/2020-12-06-w100.adoc $lang:content/$lang/post/2020-12-06-w100.adoc 

source=$(echo $1)
dest=$(echo $1 | sed -e 's/\/fr\//\/\$lang\//g')
echo
echo -e "[type: asciidoc] $1" '$lang:'"$dest" >> fr.cfg
